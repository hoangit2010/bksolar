//
//  FRATProgressIndicator.h
//  FRATapplication
//
//  Created by ved on 4/22/14.
//  Copyright (c) 2014 iNext. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FRATProgressIndicator : UIView


+(void)showLoadingLayerTo:(UIView *)view loadingMessage:(NSString *)loadingMessage;
+ (void)hideLoadingLayerForView:(UIView *)view;

@property(nonatomic, strong) IBOutlet UIView *loadingView;
@property(nonatomic, weak) IBOutlet UILabel *lblLoadingMsg;
@property(nonatomic, weak) IBOutlet UIImageView *indicatorImage;

@end
