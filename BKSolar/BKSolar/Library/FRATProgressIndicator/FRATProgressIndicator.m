//
//  FRATProgressIndicator.m
//  FRATapplication
//
//  Created by ved on 4/22/14.
//  Copyright (c) 2014 iNext. All rights reserved.
//

#import "FRATProgressIndicator.h"

#import "UIImage+animatedGIF.h"

#define IOS_OLDER_THAN_7 ( [ [ [ UIDevice currentDevice ] systemVersion ] floatValue ] < 7.0 )

@interface FRATProgressIndicator (){
    CGFloat width;
}

@end

@implementation FRATProgressIndicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self = [[[NSBundle mainBundle] loadNibNamed:@"FRATProgressIndicator" owner:self options:nil] objectAtIndex:0];
        [self registerForNotifications];
        
    }
    return self;
}

- (id)initWithView:(UIView *)view {
	NSAssert(view, @"View must not be nil.");
	return [self initWithFrame:view.bounds];
}

- (void)layoutSubviews {
	
	// Entirely cover the parent view
	UIView *parent = self.superview;
	if (parent) {
		self.frame = parent.bounds;
        [self setFrameOfSubview];
	}
}

+(void)showLoadingLayerTo:(UIView *)view loadingMessage:(NSString *)loadingMessage{
    
    FRATProgressIndicator *loadingContentView = [[self alloc] initWithView:view];
    
   // [HairSaloonUtility createLoadingBackgorundToView:loadingContentView.loadingView];
    loadingContentView.lblLoadingMsg.adjustsFontSizeToFitWidth = YES;
    if(loadingMessage){
        loadingContentView.lblLoadingMsg.text = loadingMessage;
    }
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"loader1" withExtension:@"GIF"];
    loadingContentView.indicatorImage.image = [UIImage animatedImageWithAnimatedGIFData:[NSData dataWithContentsOfURL:url]];
    [view addSubview:loadingContentView];
}

-(void)setFrameOfSubview{
    self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x,80, self.frame.size.width-20, self.loadingView.frame.size.height);
    CGRect frame = self.lblLoadingMsg.frame;
    frame.origin.x =  self.frame.size.width/4;
    
    if(IOS_OLDER_THAN_7){
      //  self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x,10, self.frame.size.width-20, self.loadingView.frame.size.height);
    }
   
    width =  [_lblLoadingMsg.text sizeWithFont:[UIFont systemFontOfSize:15 ]].width;

    frame.size.width = width;
  //  self.lblLoadingMsg.frame = frame;
  //  self.lblLoadingMsg.center = CGPointMake(self.frame.size.width/2, self.loadingView.frame.size.height/2);
  //  self.indicatorImage.frame = CGRectMake(self.lblLoadingMsg.frame.origin.x - 35, self.indicatorImage.frame.origin.y, 25, 25);
}


+ (void)hideLoadingLayerForView:(UIView *)view {
	FRATProgressIndicator *loadingContentView = [self layerForView:view];
	if (loadingContentView != nil) {
        [loadingContentView unregisterFromNotifications];
		[loadingContentView removeFromSuperview];
	}
}

- (void)unregisterFromNotifications {
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (id)layerForView:(UIView *)view {
	NSEnumerator *subviewsEnum = [view.subviews reverseObjectEnumerator];
	for (UIView *subview in subviewsEnum) {
		if ([subview isKindOfClass:self]) {
			return (FRATProgressIndicator *)subview;
		}
	}
	return nil;
}

- (void)registerForNotifications {
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(deviceOrientationDidChange:)
			   name:UIDeviceOrientationDidChangeNotification object:nil];
}


- (void)deviceOrientationDidChange:(NSNotification *)notification {
	if (self) {
        //    self.frame = CGRectMake(0,0,self.parentView.frame.size.width,self.parentView.frame.size.height);
        //    self.loadingView.frame = CGRectMake(self.loadingView.frame.origin.x,self.loadingView.frame.origin.y, self.parentView.frame.size.width -20, 53);
        [self setFrameOfSubview];
    }
	
}



@end
