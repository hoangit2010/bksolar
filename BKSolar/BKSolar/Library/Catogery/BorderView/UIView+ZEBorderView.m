//
//  UIView+ZEBorderView.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "UIView+ZEBorderView.h"

@implementation UIView (ZEBorderView)

-(void)setUpBorderView {
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 2.0f;
    self.layer.cornerRadius = 15.0f;
}

@end
