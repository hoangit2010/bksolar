//
//  UIView+ZEBorderView.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ZEBorderView)

-(void)setUpBorderView;

@end
