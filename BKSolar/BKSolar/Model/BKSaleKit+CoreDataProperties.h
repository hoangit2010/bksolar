//
//  BKSaleKit+CoreDataProperties.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BKSaleKit.h"

NS_ASSUME_NONNULL_BEGIN

@interface BKSaleKit (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *idsalekit;
@property (nullable, nonatomic, retain) NSString *iduser;
@property (nullable, nonatomic, retain) NSString *namesalekit;
@property (nullable, nonatomic, retain) NSString *descriptionsalekit;
@property (nullable, nonatomic, retain) NSString *linkpdfsalekit;
@property (nullable, nonatomic, retain) NSString *thumbnailssalekit;

@end

NS_ASSUME_NONNULL_END
