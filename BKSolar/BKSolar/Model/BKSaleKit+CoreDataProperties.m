//
//  BKSaleKit+CoreDataProperties.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "BKSaleKit+CoreDataProperties.h"

@implementation BKSaleKit (CoreDataProperties)

@dynamic idsalekit;
@dynamic iduser;
@dynamic namesalekit;
@dynamic descriptionsalekit;
@dynamic linkpdfsalekit;
@dynamic thumbnailssalekit;

@end
