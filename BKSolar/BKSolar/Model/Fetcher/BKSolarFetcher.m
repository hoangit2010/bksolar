//
//  BKSolarFetcher.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "BKSolarFetcher.h"
#import "AFNetworking.h"
#import "BKSaleKitEntity.h"
#import "BKClipEntity.h"
#import "BKMediaEntity.h"
#import "BKProjectEntity.h"

@interface BKSolarFetcher ()

@end

@implementation BKSolarFetcher

+ (BKSolarFetcher *) shareInstance {
    static dispatch_once_t once;
    static BKSolarFetcher *shareInstance;
    dispatch_once(&once, ^{
        shareInstance = [[self alloc] init];
    });
    return shareInstance;
}

- (void)beginFetcherDocumentSaleKitWithCompletionHandler:(void(^)(NSArray *array, NSError *error))completionHandler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://sunsoftplus.net/api/docs/index.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        
        NSArray *array = [[responseObject objectForKey:@"docs"] objectForKey:@"data"];
        
        for (NSDictionary *dic in array) {
            BKSaleKitEntity *entity = [[BKSaleKitEntity alloc] init];
            entity.idSaleKit = [[dic objectForKey:@"Doc"] objectForKey:@"id"];
            entity.idUser = [[dic objectForKey:@"Doc"] objectForKey:@"user_id"];
            entity.nameSaleKit = [[dic objectForKey:@"Doc"] objectForKey:@"name"];
            entity.descriptionSaleKit = [[dic objectForKey:@"Doc"] objectForKey:@"description"];
            entity.linkPDFSaleKit = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/documents/",[[dic objectForKey:@"Doc"] objectForKey:@"link"]];
           // entity.thumbnailsSalekit = [dic objectForKey:@""];
            
            [result addObject:entity];
        }
        completionHandler(result,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

// fetch data for clip solar BK
- (void)beginFetcherClipWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://sunsoftplus.net/api/videos/index.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        NSLog(@"responseObject: %@",responseObject);
        
        NSArray *array = [[responseObject objectForKey:@"videos"] objectForKey:@"data"];
        
        for (NSDictionary *dic in array) {
            BKClipEntity *entity = [[BKClipEntity alloc] init];
            entity.idClip = [[dic objectForKey:@"Video"] objectForKey:@"id"];
            entity.idUser = [[dic objectForKey:@"Video"] objectForKey:@"user_id"];
            entity.nameClip = [[dic objectForKey:@"Video"] objectForKey:@"name"];
            entity.descriptionClip = [[dic objectForKey:@"Video"] objectForKey:@"description"];
            entity.linkMp4Clip = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/videosfolder/",[[dic objectForKey:@"Video"] objectForKey:@"link"]];
             entity.thumbnailClip = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/videosfolder/",[[dic objectForKey:@"Video"] objectForKey:@"thumbnail"]];
            //entity.thumbnailClip = [[dic objectForKey:@"Video"] objectForKey:@"thumbnail"];
            [result addObject:entity];
        }
        completionHandler(result,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

// fetch data for truyen thong solar BK
- (void)beginFetcherTraditionalWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://sunsoftplus.net/api/media/index.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        
        NSLog(@"responseObject %@",responseObject);
        
        NSArray *array = [[responseObject objectForKey:@"medias"] objectForKey:@"data"];
        
        for (NSDictionary *dic in array) {
            BKMediaEntity *entity = [[BKMediaEntity alloc] init];
            entity.idMedia = [[dic objectForKey:@"Media"] objectForKey:@"id"];
            entity.idUser = [[dic objectForKey:@"Media"] objectForKey:@"user_id"];
            entity.nameMedia = [[dic objectForKey:@"Media"] objectForKey:@"name"];
            entity.descriptionMedia = [[dic objectForKey:@"Media"] objectForKey:@"description"];
            entity.linkMp4Media = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/mediasfolder/",[[dic objectForKey:@"Media"] objectForKey:@"link"]];
            entity.thumbnailMedia = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/mediasfolder/",[[dic objectForKey:@"Media"] objectForKey:@"thumbnail"]];
            //entity.thumbnailMedia = [[dic objectForKey:@"Media"] objectForKey:@"thumbnail"];
            [result addObject:entity];
            
        }
        completionHandler(result,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

// fetch data for Hot Project solar BK
- (void)beginFetcherHotProjectWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://sunsoftplus.net/api/projects/index.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        
        NSArray *array = [[responseObject objectForKey:@"projects"] objectForKey:@"data"];
        
        for (NSDictionary *dic in array) {
            BKProjectEntity *entity = [[BKProjectEntity alloc] init];
            entity.idProject = [[dic objectForKey:@"Project"] objectForKey:@"id"];
            entity.idUser = [[dic objectForKey:@"Project"] objectForKey:@"user_id"];
            entity.nameProject = [[dic objectForKey:@"Project"] objectForKey:@"name"];
            entity.descriptionProject = [[dic objectForKey:@"Project"] objectForKey:@"description"];
            //entity.thumbnailsProject = [[dic objectForKey:@"Project"] objectForKey:@"thumbnail"];
            entity.thumbnailsProject = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/projectsfolder/",[[dic objectForKey:@"Project"] objectForKey:@"thumbnail"]];
            entity.linkMp4Project = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/projectsfolder/",[[dic objectForKey:@"Project"] objectForKey:@"link"]];
            [result addObject:entity];
        }
        completionHandler(result,nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
