//
//  BKSolarFetcher.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BKSolarFetcher : NSObject

+ (BKSolarFetcher*) shareInstance;

// fetch data for SaleKit solar BK
- (void)beginFetcherDocumentSaleKitWithCompletionHandler:(void(^)(NSArray *array, NSError *error))completionHandler;

// fetch data for clip solar BK
- (void)beginFetcherClipWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler;

// fetch data for truyen thong solar BK
- (void)beginFetcherTraditionalWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler;

// fetch data for Hot Project solar BK
- (void)beginFetcherHotProjectWithCompletionHandler:(void (^)(NSArray *array, NSError *error))completionHandler;

@end
