//
//  BKMediaEntity.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BKMediaEntity : NSObject

@property(nonatomic, strong) NSString *idMedia;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *nameMedia;
@property(nonatomic, strong) NSString *descriptionMedia;
@property(nonatomic, strong) NSString *linkMp4Media;
@property(nonatomic, strong) NSString *thumbnailMedia;

@end
