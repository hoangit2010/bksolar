//
//  BKClipEntity.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BKClipEntity : NSObject
@property(nonatomic, strong) NSString *idClip;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *nameClip;
@property(nonatomic, strong) NSString *descriptionClip;
@property(nonatomic, strong) NSString *linkMp4Clip;
@property(nonatomic, strong) NSString *thumbnailClip;
@end
