//
//  BKSaleKitEntity.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BKSaleKitEntity : NSObject

@property(nonatomic, strong) NSString *idSaleKit;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *nameSaleKit;
@property(nonatomic, strong) NSString *descriptionSaleKit;
@property(nonatomic, strong) NSString *linkPDFSaleKit;
@property(nonatomic, strong) NSString *thumbnailsSalekit;

@end
