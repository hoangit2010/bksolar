//
//  BKProjectEntity.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BKProjectEntity : NSObject

@property(nonatomic, strong) NSString *idProject;
@property(nonatomic, strong) NSString *idUser;
@property(nonatomic, strong) NSString *nameProject;
@property(nonatomic, strong) NSString *descriptionProject;
@property(nonatomic, strong) NSString *linkMp4Project;
@property(nonatomic, strong) NSString *thumbnailsProject;

@end
