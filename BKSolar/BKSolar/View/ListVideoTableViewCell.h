//
//  ListVideoTableViewCell.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/23/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListVideoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *listVideoImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameVideoLabel;

@end
