//
//  ProjectCollectionViewCell.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/13/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProjectCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ProjectThumnailImage;
@property (weak, nonatomic) IBOutlet UILabel *projectLabel;
@end
