//
//  AboutUsCollectionViewCell.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *aboutUsImageView;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsLabel;

@end
