//
//  SaleKitCollectionViewCell.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/12/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleKitCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageThumails;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end
