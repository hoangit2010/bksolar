//
//  AboutUsViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "AboutUsViewController.h"
#import "UIView+ZEBorderView.h"
#import "AboutUsCollectionViewCell.h"
#import "BKSolarFetcher.h"
#import "BKMediaEntity.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "AppDelegate.h"
#import "PlayVideoViewController.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface AboutUsViewController () <UICollectionViewDataSource, UICollectionViewDelegate>
@property(nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation AboutUsViewController

- (id)init {
    if (self = [super init]) {
        self.dataArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.borderView setUpBorderView];
    // register nib
    UINib *aboutUsNib = [UINib nibWithNibName:NSStringFromClass([AboutUsCollectionViewCell class]) bundle:[NSBundle mainBundle]];
    [self.AboutUsCollectionView registerNib:aboutUsNib forCellWithReuseIdentifier:@"AboutUsCollectionViewCell"];
    [SVProgressHUD show];
    [[BKSolarFetcher shareInstance] beginFetcherTraditionalWithCompletionHandler:^(NSArray *array, NSError *error) {
        self.dataArray = array;
        [SVProgressHUD dismiss];
        [self.AboutUsCollectionView reloadData];
    }];
    self.AboutUsCollectionView.dataSource = self;
    self.AboutUsCollectionView.delegate = self;
    
}

- (IBAction)dismissAboutUsViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"AboutUsCollectionViewCell";
    AboutUsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
    BKMediaEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
//    NSString *str = entity.linkMp4Media;
//    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    CMTime time = [asset duration];
//    time.value = 20;
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    
    if ([entity.thumbnailMedia isKindOfClass:[NSNull class]]) {
        cell.aboutUsImageView.image = [UIImage imageNamed:@"placeholder.png"];
    }else{
        [cell.aboutUsImageView sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailMedia] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    
    cell.aboutUsLabel.text = entity.nameMedia;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BKMediaEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.strVideoURL = entity.linkMp4Media;
    app.titleName = entity.nameMedia;
    app.typeVideo = 2;
    app.arrayVideo = self.dataArray;
    [app.arrayVideo removeObjectAtIndex:indexPath.row];
    PlayVideoViewController *playVideoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVideoViewController"];
    [self presentViewController:playVideoViewController animated:YES completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.AboutUsCollectionView.frame.size.width/2.0-7
                      , self.AboutUsCollectionView.frame.size.width*0.6);
}

@end
