//
//  PlayVideoViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/13/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "PlayVideoViewController.h"
#import "AppDelegate.h"
#import <MediaPlayer/MediaPlayer.h>
#import "AFNetworking.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "ALAssetsLibrary+CustomPhotoAlbum.h"
#import "ListVideoTableViewCell.h"
#import "BKClipEntity.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "BKMediaEntity.h"
#import "BKProjectEntity.h"

@interface PlayVideoViewController ()<UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSString *strVideoURL;
@property (nonatomic,strong) MPMoviePlayerController *videoPlayer;
@property (nonatomic, strong) UILocalNotification *currentNotification;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) AppDelegate *app;


@property (strong, atomic) ALAssetsLibrary* library;
@end

@implementation PlayVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.library = [[ALAssetsLibrary alloc] init];
    // Do any additional setup after loading the view.
    self.app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self.titleLabel.text = self.app.titleName;
    self.strVideoURL = [NSString stringWithFormat:@"%@",self.app.strVideoURL];
    NSURL *URL=[NSURL URLWithString:[self.strVideoURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    UINib *nib = [UINib nibWithNibName:@"ListVideoTableViewCell" bundle:nil];
    [self.listVideoTableView registerNib:nib forCellReuseIdentifier:@"ListVideoTableViewCell"];
    [self playVideoWithURL:URL];
}
/*
 METHOD :   playVideo
 PARAM  :   nill
 PURPOSE:   Play selected video
 RETURN :   nill
 EXCEPTION: nill
 */
-(void)playVideoWithURL:(NSURL*)url
{
    
    self.videoPlayer=[[MPMoviePlayerController alloc]initWithContentURL:url];
    [self.videoPlayer setScalingMode:MPMovieScalingModeAspectFill];
    [self.videoPlayer.view setFrame:CGRectMake(0,0, self.view.frame.size.width,self.playVideoView.frame.size.height)];
    [self.playVideoView addSubview:self.videoPlayer.view];
    self.videoPlayer.shouldAutoplay=YES;
   
    [self.videoPlayer play];
}

- (IBAction)dismissPlayVideoViewController:(id)sender {
    [self.videoPlayer stop];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UIAlerViewDelegate
- (IBAction)downLoadVideoViewController:(id)sender {

    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Tải Video"
                                          message:@"Bạn có chắc muốn tải video này ?"
                                          preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"Huỷ", @"Cancel action")
                                   style:UIAlertActionStyleCancel
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"Cancel action");
                                   }];
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:NSLocalizedString(@"Tải", @"OK action")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {
                                   [self downloadVideo];
                               }];
    
    [alertController addAction:cancelAction];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)downloadVideo {
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc]  initWithSessionConfiguration:configuration];
    NSURL *URL=[NSURL URLWithString:[self.strVideoURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    NSURLSessionDownloadTask *downloadTask = [manager downloadTaskWithRequest:request progress:nil destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
    NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        return [documentsDirectoryURL URLByAppendingPathComponent:[response suggestedFilename]];
    } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
        NSLog(@"File downloaded to: %@", URL);
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.alertBody = [NSString stringWithFormat:@"Đã tải thành công %@",self.app.titleName];
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        // Request to reload reminder table view data
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reminderDownload" object:nil];
 
        self.library = [[ALAssetsLibrary alloc] init];
        [self saveVideo:filePath toAlbum:@"BKSolar_Video" withCompletionBlock:^(NSError *error) {
            NSLog(@"error %@",[error description]);
        }];

    }];
    [downloadTask resume];

}

-(void)saveVideo:(NSURL *)videoUrl toAlbum:(NSString*)albumName withCompletionBlock:  (SaveImageCompletion)completionBlock
{
    //write the image data to the assets library (camera roll)
    [self.library writeVideoAtPathToSavedPhotosAlbum:videoUrl completionBlock:^(NSURL* assetURL, NSError* error) {
        NSLog(@"error: %@", [error description]);
        //error handling
        if (error!=nil) {
            completionBlock(error);
            return;
        }
        
        //add the asset to the custom photo album
        [self.library addAssetURL: assetURL
                  toAlbum:albumName
      withCompletionBlock:completionBlock];
        
    }];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.app.arrayVideo.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"ListVideoTableViewCell";
    ListVideoTableViewCell *cell = [self.listVideoTableView dequeueReusableCellWithIdentifier:identify];
    //BKClipEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
    //cell.nameVideoLabel.text = element objectForKey:@"";
   // [cell.listVideoImageView sd_setImageWithURL:[NSURL URLWithString:entity.linkMp4Clip] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    if (self.app.typeVideo == 1) {
        // clip
        BKClipEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        cell.nameVideoLabel.text = entity.nameClip;
        if ([entity.linkMp4Clip isKindOfClass:[NSNull class]]) {
            cell.listVideoImageView.image = [UIImage imageNamed:@"placeholder.png"];
        }else{
            [cell.listVideoImageView sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailClip] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
    } else if (self.app.typeVideo == 2) {
        // about
        
        BKMediaEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        cell.nameVideoLabel.text = entity.nameMedia;
        if ([entity.linkMp4Media isKindOfClass:[NSNull class]]) {
            cell.listVideoImageView.image = [UIImage imageNamed:@"placeholder.png"];
        }else{
            [cell.listVideoImageView sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailMedia] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
    } else {
       // project
        BKProjectEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        cell.nameVideoLabel.text = entity.nameProject;
        if ([entity.linkMp4Project isKindOfClass:[NSNull class]]) {
            cell.listVideoImageView.image = [UIImage imageNamed:@"placeholder.png"];
        }else{
            [cell.listVideoImageView sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailsProject] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.listVideoTableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.app.typeVideo == 1) {
        // clip
        BKClipEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        [self playVideoWithURL:[NSURL URLWithString:[entity.linkMp4Clip stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
         self.titleLabel.text = entity.nameClip;
    } else if (self.app.typeVideo == 2) {
        // about
        BKMediaEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        [self playVideoWithURL:[NSURL URLWithString:[entity.linkMp4Media stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
         self.titleLabel.text = entity.nameMedia;
    } else {
        // project
        BKProjectEntity *entity = [self.app.arrayVideo objectAtIndex:indexPath.row];
        [self playVideoWithURL:[NSURL URLWithString:[entity.linkMp4Project stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
        self.titleLabel.text = entity.nameProject;
    }
}
@end
