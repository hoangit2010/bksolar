//
//  PlayVideoViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/13/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface PlayVideoViewController : UIViewController
- (IBAction)dismissPlayVideoViewController:(id)sender;
- (IBAction)downLoadVideoViewController:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *playVideoView;
@property (weak, nonatomic) IBOutlet UITableView *listVideoTableView;

@end
