//
//  HotProjectViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "HotProjectViewController.h"
#import "UIView+ZEBorderView.h"
#import "ProjectCollectionViewCell.h"
#import "BKSolarFetcher.h"
#import "BKProjectEntity.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "PlayVideoViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface HotProjectViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property(nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation HotProjectViewController

// init data
- (id)init {
    if (self = [super init]) {
        self.dataArray = [[NSMutableArray alloc]init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.borderView setUpBorderView];
    // register nib
    UINib *aboutUsNib = [UINib nibWithNibName:NSStringFromClass([ProjectCollectionViewCell class]) bundle:[NSBundle mainBundle]];
    [self.ProjectCollectionView registerNib:aboutUsNib forCellWithReuseIdentifier:@"ProjectCollectionViewCell"];
    
    [SVProgressHUD show];
    [[BKSolarFetcher shareInstance] beginFetcherHotProjectWithCompletionHandler:^(NSArray *array, NSError *error) {
        self.dataArray = [array mutableCopy];
        [SVProgressHUD dismiss];
        [self.ProjectCollectionView reloadData];
    }];
    self.ProjectCollectionView.dataSource = self;
    self.ProjectCollectionView.delegate = self;
    
}

- (IBAction)dismissHotProjectViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"ProjectCollectionViewCell";
    ProjectCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    
    BKProjectEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
//    NSString *str = entity.linkMp4Project;
//    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    // get thumnails
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    CMTime time = [asset duration];
//    time.value = 20;
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    if ([entity.thumbnailsProject isKindOfClass:[NSNull class]]) {
        cell.ProjectThumnailImage.image = [UIImage imageNamed:@"placeholder.png"];
    }else{
        [cell.ProjectThumnailImage sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailsProject] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    cell.projectLabel.text = entity.nameProject;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BKProjectEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.strVideoURL = entity.linkMp4Project;
    app.titleName = entity.nameProject;
    app.typeVideo = 3;
    app.arrayVideo = self.dataArray;
    [app.arrayVideo removeObjectAtIndex:indexPath.row];
    PlayVideoViewController *playVideoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVideoViewController"];
    [self presentViewController:playVideoViewController animated:YES completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.ProjectCollectionView.frame.size.width/2.0-7
                      , self.ProjectCollectionView.frame.size.width*0.6);
}
@end
