//
//  HotProjectViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotProjectViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UICollectionView *ProjectCollectionView;

- (IBAction)dismissHotProjectViewController:(id)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicatorView;
@end
