//
//  HomeViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/7/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "HomeViewController.h"
#import "KASlideShow.h"
#import <QuartzCore/QuartzCore.h>

@interface HomeViewController () <KASlideShowDelegate>

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setUpSlideShow];
    [self setUpPageControl];
  //  [self setUpButton];
    [self.detailButton.layer setBorderWidth:1.0f];
    self.detailButton.layer.cornerRadius = 5.0f;
    [self.detailButton.layer setBorderColor:[UIColor whiteColor].CGColor];
}
- (void)setUpSlideShow {
    self.slideShow.delegate = self;
    [self.slideShow setDelay:3]; // Delay between transitions
    [self.slideShow setTransitionDuration:.5]; // Transition duration
    [self.slideShow setTransitionType:KASlideShowTransitionSlide];
    [self.slideShow setImagesContentMode:UIViewContentModeScaleAspectFill];
    [self.slideShow addGesture:KASlideShowGestureSwipe];
    
    [self.slideShow addImagesFromResources:@[@"HoSoNangLuc",@"2.png", @"3.png"]];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0
                                     target:self
                                   selector:@selector(autoTransitionImage:)
                                   userInfo:nil
                                    repeats:YES];
}

- (void)setUpPageControl {
    self.fxPageControl.numberOfPages = 3;
    self.fxPageControl.dotSize = 17;
}

- (void)autoTransitionImage:(NSTimer*)t {
    [self.slideShow next];
}

#pragma mark - KASlideShow delegate

- (void)kaSlideShowWillShowNext:(KASlideShow *)slideShow
{
    self.fxPageControl.currentPage = self.slideShow.currentIndex;
}

- (void)kaSlideShowWillShowPrevious:(KASlideShow *)slideShow
{
    self.fxPageControl.currentPage = self.slideShow.currentIndex;
}

- (void) kaSlideShowDidShowNext:(KASlideShow *)slideShow
{
    self.fxPageControl.currentPage = self.slideShow.currentIndex;
}

-(void)kaSlideShowDidShowPrevious:(KASlideShow *)slideShow
{
    //NSLog(@"kaSlideShowDidPrevious, index : %@",@(slideShow.currentIndex));
    self.fxPageControl.currentPage = self.slideShow.currentIndex;
}
//}

@end
