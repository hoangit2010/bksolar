//
//  HomeViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/7/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"
#import "FXPageControl.h"

@interface HomeViewController : UIViewController
@property (strong, nonatomic) IBOutlet KASlideShow *slideShow;
@property (weak, nonatomic) IBOutlet FXPageControl *fxPageControl;
@property (weak, nonatomic) IBOutlet UIButton *detailButton;

@end
