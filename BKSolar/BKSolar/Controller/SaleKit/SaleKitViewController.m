//
//  SaleKitViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/10/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "SaleKitViewController.h"
#import "UIView+ZEBorderView.h"
#import "SaleKitCollectionViewCell.h"
#import "BKSolarFetcher.h"
#import "Reachability.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "BKSaleKitEntity.h"
#import "BKSaleKit.h"
#import "ReaderDocument.h"
#import "ReaderViewController.h"
#import "AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SVProgressHUD.h"

@interface SaleKitViewController ()<ReaderViewControllerDelegate,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
{
    NSTimer *timer;
}
@property(nonatomic, strong) NSMutableArray *saleKitArray;
@property(nonatomic, weak) NSString *strPDFName;
@property(nonatomic, weak) NSString *strPDF;

@end

@implementation SaleKitViewController

/*
 METHOD :   connected
 PARAM  :   nill
 PURPOSE:   check internet connectivity with Reachability class
 RETURN :   nill
 EXCEPTION: nill
 */
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[self setUpBorderView];
    [self.borderView setUpBorderView];
    // register nibs
    UINib *saleKitNib = [UINib nibWithNibName:NSStringFromClass([SaleKitCollectionViewCell class]) bundle:[NSBundle mainBundle]];
    [self.saleKitCollectionView registerNib:saleKitNib forCellWithReuseIdentifier:@"SaleKitCollectionViewCell"];
    // delegate, datasource
    self.saleKitCollectionView.delegate = self;
    self.saleKitCollectionView.dataSource = self;
    if (![self connected]) {
        [self FetchBKSaleKitDataFromCoreData];
    } else {
        [self BackgroundRequestForEmagazineData];
    }
}

- (void)BackgroundRequestForEmagazineData {
    [SVProgressHUD show];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"http://sunsoftplus.net/api/docs/index.json" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *array = [[responseObject objectForKey:@"docs"] objectForKey:@"data"];
        [SVProgressHUD dismiss];
        NSLog(@"----%@", responseObject);
        [self SaveBKSaleKitDataToCoreData:array];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [SVProgressHUD dismiss];
    }];
}
/*
 METHOD :   SaveBKSaleKitDataToCoreData
 PARAM  :   (NSArray *)arrays
 PURPOSE:   Save NEW BKSaleKitData data to core data (SQLite database)
 RETURN :   nill
 EXCEPTION: nill
 */
-(void)SaveBKSaleKitDataToCoreData:(NSArray*)arrays
{
    [SVProgressHUD show];
    if (arrays.count != 0) {
        
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSEntityDescription *entity = [NSEntityDescription
                                       entityForName:@"BKSaleKit" inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        
        NSError *error = nil;
        if (!error) {
            AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            NSManagedObjectContext *context = [appDelegate managedObjectContext];
            
            NSFetchRequest *allevents = [[NSFetchRequest alloc] init];
            [allevents setEntity:[NSEntityDescription entityForName:@"BKSaleKit" inManagedObjectContext:context]];
            NSArray *allCons = [context executeFetchRequest:allevents error:&error];
            for (BKSaleKit *tmpCon in allCons)
            {
                [context deleteObject:tmpCon];
            }
            [context save:&error];
            [context reset];
            
            NSUInteger count1 = [context countForFetchRequest:fetchRequest
                                                        error:&error];
            
            if (count1 == 0) {
                AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                NSManagedObjectContext *context = [appDelegate managedObjectContext];
                
                for (id element in arrays) {
                    BKSaleKit *bksaleKit = [NSEntityDescription
                                            insertNewObjectForEntityForName:@"BKSaleKit"
                                            inManagedObjectContext:context];
                    bksaleKit.idsalekit = [[element objectForKey:@"Doc"] objectForKey:@"id"];
                    bksaleKit.iduser = [[element objectForKey:@"Doc"] objectForKey:@"user_id"];
                    bksaleKit.namesalekit = [[element objectForKey:@"Doc"] objectForKey:@"name"];
                    bksaleKit.descriptionsalekit = [[element objectForKey:@"Doc"] objectForKey:@"description"];
                  //  bksaleKit.thumbnailssalekit = [[element objectForKey:@"Doc"] objectForKey:@"thumnail"];
                    bksaleKit.thumbnailssalekit = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/documents/",[[element objectForKey:@"Doc"] objectForKey:@"thumbnail"]];
                    bksaleKit.linkpdfsalekit = [NSString stringWithFormat:@"%@%@",@"http://sunsoftplus.net/documents/",[[element objectForKey:@"Doc"] objectForKey:@"link"]];
                }
                NSError *error;
                if (![context save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                }
                else
                {
                    [self FetchBKSaleKitDataFromCoreData];
                }
            }
        }
        else{
            NSLog(@"No data!");
        }
    }else{
       [self FetchBKSaleKitDataFromCoreData];
    }
}

- (void)FetchBKSaleKitDataFromCoreData {
    [SVProgressHUD show];
    self.saleKitArray = [[NSMutableArray alloc] init];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"BKSaleKit" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    [fetchRequest setResultType:NSDictionaryResultType];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
  
    for (id info in fetchedObjects) {
        [self.saleKitArray addObject:info];
    }
      [SVProgressHUD dismiss];
    [self.saleKitCollectionView reloadData];
}

- (IBAction)dismissSaleKitViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma UICollectionViewDataSource, UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.saleKitArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *identify = @"SaleKitCollectionViewCell";
    SaleKitCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
    NSDictionary *bksaleKit= [self.saleKitArray objectAtIndex:indexPath.row];
    NSString *thumbnails = [bksaleKit objectForKey:@"thumbnailssalekit"];
    [cell.imageThumails sd_setImageWithURL:[NSURL URLWithString:thumbnails] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    cell.descriptionLabel.text = [bksaleKit objectForKey:@"namesalekit"];
    return cell;
}

/*
 METHOD :   openPDF
 PARAM  :   nill
 PURPOSE:   Open selected PDF in PDF Viewer
 RETURN :   nill
 EXCEPTION: nill
 */

-(void)openPDFWidth {
    [SVProgressHUD show];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.strPDF = [NSString stringWithFormat:@"%@",appDelegate.strPDFURL];
    self.strPDFName = appDelegate.strPDFName;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSData *pdfdata = [[NSData alloc]initWithContentsOfURL:[NSURL URLWithString:self.strPDF]];
    // documentsDirectory
    NSString *documentsDirectory = [paths objectAtIndex:0];
    // strPath
    NSString *strPath = [NSString stringWithFormat:@"%@.pdf", self.strPDFName];
    // filePath
    NSString *filepath = [documentsDirectory stringByAppendingPathComponent:strPath];
    // write file with path
    [pdfdata writeToFile:filepath atomically:YES];
    NSFileManager *fileManager = [NSFileManager defaultManager ];
    BOOL isDir;
    BOOL fileExists = [fileManager fileExistsAtPath:filepath isDirectory:&isDir];
    if (fileExists)
    {
        [SVProgressHUD show];
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filepath password:nil];
        ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
        
        readerViewController.delegate = self; // Set the ReaderViewController delegate to self
        readerViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        readerViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [SVProgressHUD dismiss];
        [self presentViewController:readerViewController animated:YES completion:NULL];
    }else{
        NSLog(@"ko ton tai");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Dữ liệu chưa được tải về" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [SVProgressHUD dismiss];
    }
    [SVProgressHUD dismiss];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [SVProgressHUD show];
      timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadWS) userInfo:nil repeats:NO];
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];;
    NSDictionary *bksaleKit= [self.saleKitArray objectAtIndex:indexPath.row];
    appDelegate.strPDFName = [bksaleKit objectForKey:@"namesalekit"];
    appDelegate.strPDFURL = [bksaleKit objectForKey:@"linkpdfsalekit"];
  //  [self openPDFWidth];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.saleKitCollectionView.frame.size.width/2.0-7
                      , self.saleKitCollectionView.frame.size.width*0.6);
}

- (void)loadWS
{
    [SVProgressHUD show];
    [timer invalidate];
    timer = nil;
    [self openPDFWidth];
    [SVProgressHUD dismiss];
}

@end
