//
//  SaleKitViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/10/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SaleKitViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *borderView;
- (IBAction)dismissSaleKitViewController:(id)sender;
@property (weak, nonatomic) IBOutlet UICollectionView *saleKitCollectionView;

@end
