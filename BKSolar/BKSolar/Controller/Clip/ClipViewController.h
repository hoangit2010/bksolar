//
//  ClipViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClipViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UICollectionView *clipCollectionView;

- (IBAction)dismissClipVIewController:(id)sender;
@end
