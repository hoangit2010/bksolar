//
//  ClipViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/11/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "ClipViewController.h"
#import "UIView+ZEBorderView.h"
#import "ClipViewController.h"
#import "ClipCollectionViewCell.h"
#import "BKSolarFetcher.h"
#import "BKClipEntity.h"
#import <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>
#import "PlayVideoViewController.h"
#import "AppDelegate.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SVProgressHUD.h"
#import "PlayVideoViewController.h"

@interface ClipViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, strong) NSMutableArray *dataArray;
@property(nonatomic, strong) PlayVideoViewController *playVC;
@end

@implementation ClipViewController

- (id) init {
    if (self = [super init]) {
        self.dataArray = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
    [self.borderView setUpBorderView];
    
    // register nib
    UINib *saleKitNib = [UINib nibWithNibName:NSStringFromClass([ClipCollectionViewCell class]) bundle:[NSBundle mainBundle]];
    [self.clipCollectionView registerNib:saleKitNib forCellWithReuseIdentifier:@"ClipCollectionViewCell"];
    //
    [SVProgressHUD show];
    [[BKSolarFetcher shareInstance] beginFetcherClipWithCompletionHandler:^(NSArray *array, NSError *error) {
        self.dataArray = array;
        [SVProgressHUD dismiss];
        [self.clipCollectionView reloadData];
        
    }];
    self.clipCollectionView.delegate = self;
    self.clipCollectionView.dataSource = self;
}

- (IBAction)dismissClipVIewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource, UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identify = @"ClipCollectionViewCell";
    ClipCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identify forIndexPath:indexPath];
 
    BKClipEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    NSString *str = entity.linkMp4Clip;
//    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
//    
//    AVAsset *asset = [AVAsset assetWithURL:url];
//    
//    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
//    CMTime time = [asset duration];
//    time.value = 20;
//    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
//    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
//    
//    if (thumbnail == nil) {
//        cell.thumnailsImage.image = thumbnail;
//    }else{
//        [cell.thumnailsImage sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailClip] placeholderImage:thumbnail];
//    }
    if ([entity.thumbnailClip isKindOfClass:[NSNull class]]) {
        cell.thumnailsImage.image = [UIImage imageNamed:@"placeholder.png"];
    }else{
        [cell.thumnailsImage sd_setImageWithURL:[NSURL URLWithString:entity.thumbnailClip] placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
    
    cell.nameLabel.text = entity.nameClip;
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BKClipEntity *entity = [self.dataArray objectAtIndex:indexPath.row];
    AppDelegate *app = (AppDelegate*)[UIApplication sharedApplication].delegate;
    app.strVideoURL = entity.linkMp4Clip;
    app.titleName = entity.nameClip;
    app.arrayVideo = self.dataArray;
    app.typeVideo = 1;
    [app.arrayVideo removeObjectAtIndex:indexPath.row];
    PlayVideoViewController *playVideoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PlayVideoViewController"];
    [self presentViewController:playVideoViewController animated:YES completion:nil];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.clipCollectionView.frame.size.width/2.0-7
                      , self.clipCollectionView.frame.size.width*0.6);
}
@end
