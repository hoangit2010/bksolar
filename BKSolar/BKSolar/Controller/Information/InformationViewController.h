//
//  InformationViewController.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/8/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InformationViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *clipCompanyButton;
@property (weak, nonatomic) IBOutlet UIView *borderView;

- (IBAction)goToSaleKitViewController:(id)sender;

- (IBAction)goToClipViewController:(id)sender;

- (IBAction)goToAboutUsViewController:(id)sender;

- (IBAction)goToUsProjectViewController:(id)sender;

@end
