//
//  InformationViewController.m
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/8/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import "InformationViewController.h"
#import "SaleKitViewController.h"
#import "UIView+ZEBorderView.h"
#import "ClipViewController.h"
#import "AboutUsViewController.h"
#import "HotProjectViewController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.borderView setUpBorderView];
}

- (IBAction)goToSaleKitViewController:(id)sender {
    SaleKitViewController *saleKitViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SaleKitViewController"];
    [self presentViewController:saleKitViewController animated:YES completion:nil];

}

- (IBAction)goToClipViewController:(id)sender {
    ClipViewController *clipViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ClipViewController"];
    [self presentViewController:clipViewController animated:YES completion:nil];
}

- (IBAction)goToAboutUsViewController:(id)sender {
    AboutUsViewController *aboutUsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
    [self presentViewController:aboutUsViewController animated:YES completion:nil];
}

- (IBAction)goToUsProjectViewController:(id)sender {
    HotProjectViewController *hotProjectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HotProjectViewController"];
    [self presentViewController:hotProjectViewController animated:YES completion:nil];
}
@end
