//
//  AppDelegate.h
//  BKSolar
//
//  Created by Hoang NGUYEN on 6/4/16.
//  Copyright © 2016 Hoang NGUYEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlayVideoViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, strong) NSString *strPDFURL;
@property(nonatomic, strong) NSString *strPDFName;
@property(nonatomic, strong) NSString *strVideoURL;
@property(nonatomic, strong) NSString *titleName;
@property(nonatomic, strong) NSMutableArray *arrayVideo;
@property(nonatomic) NSInteger typeVideo;

// allow core data
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

